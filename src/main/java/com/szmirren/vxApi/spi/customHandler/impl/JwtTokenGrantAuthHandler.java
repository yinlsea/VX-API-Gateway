package com.szmirren.vxApi.spi.customHandler.impl;

import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.microsoft.sqlserver.jdbc.StringUtils;
import com.szmirren.vxApi.core.common.VxApiGatewayAttribute;
import com.szmirren.vxApi.core.enums.ContentTypeEnum;
import com.szmirren.vxApi.spi.common.HttpHeaderConstant;
import com.szmirren.vxApi.spi.customHandler.VxApiCustomHandler;
import com.szmirren.vxApi.spi.handler.VxApiAfterHandler;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

/**
 * 获得服务器时间戳
 * 
 * @author <a href="http://szmirren.com">Mirren</a>
 *
 */
public class JwtTokenGrantAuthHandler implements VxApiCustomHandler {
	/**
	 * 返回结果定义,默认占位符为$(val)
	 */
	private String resultFormat = "$(val)";
	/**
	 * 路由器要结束了还是讲任务传到下一个处理器,默认为结束
	 */
	private boolean isNext = false;
	/**
	 * 默认的返回类型为json_utf8
	 */
	private String contentType = ContentTypeEnum.JSON_UTF8.val();

	@Override
	public void handle(RoutingContext rct) {
	    String username = rct.request().params().get("username");
	    if (StringUtils.isEmpty(username)) username = rct.request().headers().get("username");
	    String secret = rct.request().params().get("secret");
	    if (StringUtils.isEmpty(secret)) secret = rct.request().headers().get("secret");
	    
	    String token = getToken(username,secret);
	    JsonObject json = new JsonObject();
	    json.put("apiToken", token);
	    json.put("apiUser", username);
		String result = resultFormat.replace("$(val)", json.toString());
		if (isNext) {
			rct.put(VxApiAfterHandler.PREV_IS_SUCCESS_KEY, Future.<Boolean>succeededFuture(true));// 告诉后置处理器当前操作成功执行
			rct.response().putHeader(HttpHeaderConstant.SERVER, VxApiGatewayAttribute.FULL_NAME).putHeader(HttpHeaderConstant.CONTENT_TYPE,
					contentType);
			rct.next();
		} else {
			rct.response().putHeader(HttpHeaderConstant.SERVER, VxApiGatewayAttribute.FULL_NAME)
					.putHeader(HttpHeaderConstant.CONTENT_TYPE, contentType).end(result);
		}
	}

	/**
	 * 实例化一个返回服务器时间戳处理器<br>
	 * option.resultFormat 格式化返回值,$(val)为值占位,默认值$(val)<br>
	 * option.isNext 是否还有后置处理器,默认false, true处理请求后next,false响应请求<br>
	 * option.contentType 返回的content-type 类型 <@ ContentTypeEnum.JSON_UTF8><br>
	 * 
	 * @param obj
	 */
	public JwtTokenGrantAuthHandler(JsonObject option) {
		if (option.getValue("resultFormat") instanceof String) {
			this.resultFormat = option.getString("resultFormat");
		}
		if (option.getValue("isNext") instanceof Boolean) {
			this.isNext = option.getBoolean("isNext");
		}
		if (option.getValue("contentType") instanceof String) {
			this.contentType = option.getString("contentType");
		}
	}

	public String getToken(String username,String secret) {
        String token = null;
        try {
            Date expiresAt = new Date(System.currentTimeMillis() + 3L * 60L * 3600L * 1000L);
            token = JWT.create().withClaim("username",username).withExpiresAt(expiresAt).sign(Algorithm.HMAC256(secret));
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return token;
    }
}
