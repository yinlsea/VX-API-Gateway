/**
 * FileName:	VxApiStart.java
 * Author:		yinl
 * Date:		2018年8月8日 下午5:39:02
 * Description:	
 * History:
 * <author>		<time>			<version>		<description>
 * 
 */
package com.szmirren.vxApi.core;

/**
 * @author		yinl
 * @description	
 * 
 */
public class VxApiBoot
{

    /**
     * @description	
     * @param args
     */
    public static void main(String[] args)
    {
        VxApiLauncher.start();
    }

}
