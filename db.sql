------ Oracle
-- Create table
create table VX_API_APPLICATION
(
  name           VARCHAR2(60) not null,
  content   	 VARCHAR2(2000)
)
tablespace DATA_SPC
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 8K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create unique index PK_VX_API_APPLICATION on VX_API_APPLICATION (name)
  tablespace INDX_SPC
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );


-- Create table
create table VX_API_APIS
(
  name           VARCHAR2(60) not null,
  app_name       VARCHAR2(60) not null,
  content   	 VARCHAR2(2000)
)
tablespace DATA_SPC
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 8K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create unique index PK_VX_API_APIS on VX_API_APIS (name,app_name)
  tablespace INDX_SPC
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );


-- Create table
create table VX_API_BLACKLIST
(
  name           VARCHAR2(60) not null,
  content   	 VARCHAR2(2000)
)
tablespace DATA_SPC
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 8K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create unique index PK_VX_API_BLACKLIST on VX_API_BLACKLIST (name)
  tablespace INDX_SPC
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );